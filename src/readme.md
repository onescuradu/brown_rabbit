# Brown Rabbit - HTML24

Author: Onescu Radu Mihai (<https://raduonescu.com>)

## Note

- Bonus requirements have been completed.

* Post Section is created to work dynamically by fetching an API or JSON file.

  Unfortunately fetching an API or a JSON file requires to serve the website from a server as browsers do not allow fetching for security reasons, therefore I have commented the lines that were used for fetching and I hardcoded the posts data inside an array.

  If you are using a server to serve the website, you can uncomment lines 208-217 and comment lines 362-363.
