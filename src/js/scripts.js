$(document).ready(function() {
  //----- START Search Bar Script
  // Search bar input
  let input = document.getElementById('search-bar');

  // Initialize a timeout
  let timeout = null;

  input.addEventListener('keyup', function(e) {
    // Clear the previous created timeout
    clearTimeout(timeout);
    // Set a timeout for 1 second
    timeout = setTimeout(function() {
      $('body').unmark({
        done: function() {
          $('body').mark(input.value, ['separateWordSearch', 'diacritics']);
        }
      });
    }, 1000);
  });
  //----- END Search Bar Script

  //----- START Slider Script
  let sliderImages = [
    'background1.jpg',
    'background2.jpg',
    'background3.jpg',
    'background4.jpg',
    'background5.jpg',
    'background6.jpg',
    'background7.jpg',
    'background8.jpg'
  ];
  //Shuffle the slider images array
  sliderImages = sliderImages.sort(() => Math.random() - 0.5);
  sliderImages.map((image, index) => {
    $('#header-slider').append(
      `<div class="carousel-item">
            <img
                class="d-block w-100"
                src="img/slider/${sliderImages[index]}"
                alt="Slide Image"
            />
        </div>`
    );
    $('.carousel-item:first').addClass('active');
    $('#carousel-indicators').append(
      `<li class="carousel-bullet" data-target="#carouselExampleIndicators" data-slide-to="3"></li>`
    );
    $('.carousel-bullet:first').addClass('active');
  });

  //----- END Slider Script

  //----- START Dynamic Post Pages Script

  //--Array storing all the posts fetched from JSON file
  let postsArray = [
    {
      image: 'post1.jpg',
      title: 'Wonderful Copenhagen',
      content:
        'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory.',
      date: '23/04-2020'
    },
    {
      image: 'post2.jpg',
      title: 'Apple and Google’s contact tracing API',
      content:
        'The first version of Apple and Google’s jointly developed, cross-platform contact tracing API should be available to developers as of next week, according to a conversation between Apple CEO Tim Cook and European Commissioner for internal market Thierry Breton. Bretton shared a photo from his office which shows him having a video conversation with Cook, and told Les Echos that the Apple chief executive told him April 28 would be the day the contact tracing API will be available to software developers building apps that employ it on behalf of public health agencies.',
      date: '23/04-2020'
    },
    {
      image: 'post3.jpg',
      title: 'Filtered coffee linked to better heart health',
      content:
        'Stay-at-home orders mean that many people are making their own morning coffee for the first time. Now, a timely new study suggests the healthiest way is with a drip coffee maker.Researchers found that coffee drinkers typically enjoyed longer lives than nondrinkers, but only if the java was filtered -- suggesting espresso lovers might be out of luck.',
      date: '23/04-2020'
    },
    {
      image: 'post4.jpg',
      title:
        'Starbucks serves 1 million free coffees to front-line workers, extends promotion through May 31',
      content:
        'Starbucks announced Thursday that the company would be extending its current free coffee offer to front-line workers and first responders through the end of May. The promotion was originally announced on March 25, with Starbucks committing to provide free, tall coffees (hot or iced) to anyone identifying as police, firefighters, paramedics or health care professionals working on the front lines amid the coronavirus crisis. At the time, Starbucks scheduled the promotion to end on May 3. On Thursday, the promotion was extended through May 31.',
      date: '23/04-2020'
    },
    {
      image: 'post5.jpg',
      title:
        'The Best Espresso Machines for Making Café-Quality Drinks at Home',
      content:
        'Ask any coffee lover and they’ll tell you there’s no better way to start the day than by drinking a warm cup of joe. But the true aficionados know that if you really need to get going, then espresso and the jolt that accompanies it is the way to go. The popular Italian method of coffee making, in which pressurized water is shot through finely ground beans to produce a more condensed style of coffee, has been around since the 1880s, but for much of that time, you had to go to a café to get a cup of high-quality espresso. This has changed in recent years, though, thanks to the widespread availability of relatively compact, electric-powered machines that allow you to take on the role of your most-trusted barista.',
      date: '23/04-2020'
    },
    {
      image: 'post6.jpg',
      title: 'Diary of a New York Cafe: Getting Coronavirus Relief Is Tough',
      content:
        'Samantha Stephens realized her longtime dream eight years ago, opening an oatmeal bar in Greenwich Village where customers can build bowls with toppings that range from chia seeds and berries to bacon and poached eggs. Now, the coronavirus has left the little company fighting for its life.',
      date: '23/04-2020'
    },
    {
      image: 'post7.jpg',
      title:
        'The Cannabis Cafe in West Hollywood reopens Thursday as a drive-thru',
      content:
        'The Cannabis Cafe in West Hollywood will reopen as a food and weed drive-thru operation starting Thursday. When the restaurant originally opened in October, people lined up around the block for the chance to smoke a joint while they ate vegan nachos. It was forced to close in March due to the coronavirus pandemic and ensuing shutdown of non-essential businesses but has since worked out a solution with the city of West Hollywood.',
      date: '23/04-2020'
    },
    {
      image: 'post8.jpg',
      title: "Greenery-covered 'parkipelago' floated for Copenhagen",
      content:
        "Copenhagen, Denmark, has its fair share of floating architecture, with BIG's floating housing and Urban Power's artificial islands being two notable examples. A project by Marshall Blecher and Fokstrot is another highlight and will add greenery-covered park-like islands to the city too. openhagen Islands is a not for profit initiative supported by Københavns Kommune, Og Havn and Den Gode Havneliv. The islands themselves will be installed in Copenhagen's south harbor and will be constructed by hand locally using traditional boat building techniques. They will consist of recycled and sustainably-sourced materials.",
      date: '23/04-2020'
    },
    {
      image: 'post9.jpg',
      title: 'Floating islands bring a new type of public park to Copenhagen',
      content:
        'Australian architect Marshall Blecher and Danish design studio Fokstrot have unveiled plans for a new type of public space in the heart of Copenhagen — a “parkipelago” of floating islands. Dubbed the Copenhagen Islands, this non-profit initiative follows the success of CPH-Ø1, the first prototype island that launched in 2018 and was anchored in various parts of the city harbor. Copenhagen Islands plans to launch three more human-made islands in 2020, with more planned in the future.',
      date: '23/04-2020'
    },
    {
      image: 'post10.jpg',
      title: 'U.S. economic aid to Greenland draws criticism in Denmark',
      content:
        'The move to improve ties with Greenland drew some criticism in Denmark, which less than a year ago rebuffed U.S. President Donald Trump’s offer to buy the vast Arctic island as “absurd.”Greenland, which on Thursday welcomed the money, is becoming increasingly important for the U.S. military and for the U.S. ballistic missile early warning system because of a Russian and Chinese commercial and military buildup in the Arctic.',
      date: '23/04-2020'
    },
    {
      image: 'post11.jpg',
      title: 'Coronavirus: Rush for haircuts in Denmark as lockdown eases',
      content:
        "Hairdressers are among small businesses allowed to reopen in Denmark Danes can now get a haircut at a salon for the first time in a month, as the Nordic country takes the next step to gradually relax its coronavirus lockdown. 'We are crazy busy. I mean fully booked for the next two weeks,' says Phil Olander, owner of Phil's Barber in central Copenhagen.",
      date: '23/04-2020'
    },
    {
      image: 'post12.jpg',
      title: 'BingeWatch: The Legacy is the Danish successor to Succession',
      content:
        'One of the first things I did before lockdown began was to set up a self-help group on WhatsApp called “TV for Hard Times”. I invited my most discerning telly-addict friends to share their favourite discoveries. My phone soon began to ping with great suggestions: The Americans, Unorthodox and Netflix newcomer Tiger King, the virtual watercooler hit of the moment. Seen. Tick. Next.',
      date: '23/04-2020'
    },
    {
      image: 'post13.jpg',
      title:
        'Centralized IT System Caused Pain For Danish Manufacturer Hit With Ransomware',
      content:
        'After Danish industrial pump manufacturer Desmi A/S was hit with a ransomware attack two weeks ago, the company’s chief executive said employees in all of its global facilities experienced outages because they use the same technology systems.',
      date: '23/04-2020'
    },
    {
      image: 'post14.jpg',
      title:
        'Danish league leaders FC Midtjylland dare to be different with drive-in plan',
      content:
        'Erik Sviatchenko, the former Celtic defender, believes that his present club has paved the way for an innovative return to football in the wake of the coronavirus, even if closed doors games will be the likely scenario. FC Midtjylland were on course to win the Danish title with a 12-point lead over current champions Copenhagen before the pandemic put everything on hold last month.',
      date: '23/04-2020'
    },
    {
      image: 'post15.jpg',
      title:
        'Olympia coffee house known for devilish drinks now does caffeine care boxes',
      content:
        "OLYMPIA, Wash. — There's a coffee shop in Olympia where they draw devilish espresso art and make drinks with names like the Chupacabra, Grave Moss... and one very popular one that we cannot say on TV or in print that's a common expletive-laden expression of frustration.",
      date: '23/04-2020'
    },
    {
      image: 'post16.jpg',
      title: 'How to make coffee: In a pot, in a French press and more',
      content:
        "In these challenging times, rituals are comforting. Whether it's putting on the same pair of house slippers every day or getting in your daily yoga session, there's something deeply therapeutic about a repeated action that you know can kick-start your day. For many people, an essential morning habit is brewing some coffee. While some bleary-eyed individuals haphazardly throw some spoonfuls of grounds into a filter basket, add water from the sink and press a button, others painstakingly weigh their grounds and monitor their water temperature to extract the perfect cup.",
      date: '23/04-2020'
    },
    {
      image: 'post17.jpg',
      title:
        'Take $150 off this serious espresso machine and make coffee-shop drinks at home',
      content:
        "Coffee shops may be mostly closed, but that doesn't mean you can't recreate the lattes, cappuccinos and other espresso drinks you've been missing at home. Best Buy currently has a professional Brim 19-Bar espresso system down $150 to just $250 for today only. For an espresso drinker, it feels like the perfect treat-yourself gift at the perfect treat-yourself moment (or maybe treat Mom?). There's also a solid 10-piece Cuisinart cookware set down to just $80: an especially good pick if you're starting from scratch, outfitting a new home (or second home) or kitchen, and need all the pots and pans. Let's take a closer look at both.",
      date: '23/04-2020'
    },
    {
      image: 'post18.jpg',
      title: 'Mother’s Day Gift Guide: The Best Gifts For The Coffee Lover',
      content:
        'A French Press is the preferred method of making coffee for many caffeine enthusiasts and this brew kit ($50) from Brooklyn Roasting Company comes with two 12oz tins of their most popular coffees paired with an 8-cup Bodum Chambord French Press. This will provide a seemingly endless supply of caffeine!',
      date: '23/04-2020'
    }
  ];
  let currentPage = 1;
  const numberOfPosts = 3;
  let totalPages = 1;

  //--DOM components for the page numbering
  let currentPageLabel = $('#current-page-label');
  let maxPageLabel = $('#max-page-label');
  let pageBtn1 = $('#page-btn-1');
  let pageBtn2 = $('#page-btn-2');
  let pageBtn3 = $('#page-btn-3');

  // Load Projects from the JSON File
  // Only works if the website is running on a server, as the browsers are blocking access to local file system
  // Therefore I hardcoded the content in the array for demonstration purposes
  // Uncomment lines 208 to 217 and comment 363 and 364 if you are using a live server to run the website
  // fetch('posts.json', { mode: 'no-cors' })
  //   .then(response => {
  //     return response.json();
  //   })
  //   .then(data => {
  //     postsArray = []; //clear the hardcoded content of the array
  //     data.posts.map(post => postsArray.push(post));
  //     addPostsToPage();
  //     totalPages = calculateTotalPages();
  //   });

  //Add posts to the page
  const addPostsToPage = () => {
    const startingPost = currentPage * numberOfPosts - numberOfPosts;
    const endingPost = currentPage * numberOfPosts;
    $('#posts-container').empty();
    for (let i = startingPost; i < endingPost; i++) {
      if (postsArray[i])
        $('#posts-container').append(
          `<article>
          <div class="row mb-4">
            <div class="col-3">
              <img
                src="img/posts/${postsArray[i].image}"
                class="post-image img-fluid"
                alt="Post Image"
              />
            </div>
            <div class="col-9">
              <h3>${postsArray[i].title}</h3>
              <h6 class="post-date font-light-blue">Posted: ${postsArray[i].date}</h6>
              <div class="post-content post-more">
               ${postsArray[i].content}
              </div>
            </div>
          </div>
          <hr />
        </article>`
        );
    }
    addReadMoreButton();
    handleReadMoreButton();
    handlePageButtonsDesign();
  };

  //Calculate Number of Total Pages
  const calculateTotalPages = () => Math.ceil(postsArray.length / 3);

  //Handle Page Buttons Design
  const handlePageButtonsDesign = currentPageNumber => {
    currentPageLabel.text(currentPage);
    maxPageLabel.text(totalPages);
    switch (currentPage) {
      case 1:
        pageBtn1.text(currentPage);

        pageBtn1[0].dataset.page = currentPage;
        pageBtn1.addClass('page-selected');

        pageBtn2.text(currentPage + 1);
        pageBtn2[0].dataset.page = currentPage + 1;

        pageBtn3.text(currentPage + 2);
        pageBtn3[0].dataset.page = currentPage + 2;
        break;
      case totalPages:
        pageBtn1.text(currentPage - 2);
        pageBtn1[0].dataset.page = currentPage - 2;

        pageBtn2.text(currentPage - 1);
        pageBtn2[0].dataset.page = currentPage - 1;

        pageBtn3.text(currentPage);
        pageBtn3[0].dataset.page = currentPage;
        pageBtn3.addClass('page-selected');
        break;
      default:
        pageBtn1.text(currentPage - 1);
        pageBtn1[0].dataset.page = currentPage - 1;

        pageBtn2.text(currentPage);
        pageBtn2[0].dataset.page = currentPage;

        pageBtn3.text(currentPage + 1);
        pageBtn3[0].dataset.page = currentPage + 1;
        pageBtn2.addClass('page-selected');
    }
  };

  //Handle Page Button Click
  $('.btn-post-page').click(function() {
    $('.page-selected').removeClass('page-selected');
    if ($(this).data('page') === 'last') {
      currentPage = totalPages;
    } else {
      currentPage = Number(this.dataset.page);
    }
    addPostsToPage();
  });

  //----- END Dynamic Post Pages Script

  //Post Read More Script
  const addReadMoreButton = () => {
    const numberOfChars = 275;
    $('.post-more').each(function() {
      var content = $(this).html();

      if (content.length > numberOfChars) {
        const smallContent = content.substr(0, numberOfChars);
        const moreContent = content.substr(
          numberOfChars - 1,
          content.length - numberOfChars
        );
        const html = `${smallContent}
          <span>...</span>
          <span class="post-more-content">
            <span>${moreContent}</span>
            <div class="mt-2">
            <a href="#" class="btn-read-more post-more-link">Read More</a>
            </div>
          </span>`;

        $(this).html(html);
      }
    });
  };

  //Click functionality for the read more button
  const handleReadMoreButton = () => {
    $('.post-more-link').click(function() {
      if ($(this).hasClass('post-less')) {
        $(this).removeClass('post-less');
        $(this).html('Read more');
      } else {
        $(this).addClass('post-less');
        $(this).html('Read less');
      }
      $(this)
        .parent()
        .parent()
        .prev()
        .toggle();
      $(this)
        .parent()
        .prev()
        .toggle();
      return false;
    });
  };

  //----- END Post Read More Script

  //Loading the posts into the page
  totalPages = calculateTotalPages();
  addPostsToPage();
});
